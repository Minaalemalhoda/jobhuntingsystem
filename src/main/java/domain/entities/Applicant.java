package domain.entities;

import java.util.Date;

public class Applicant extends User {
     int id;
     long personNumber;
     Resume resume;

     public Applicant(RoleType roleType, String userName, String password, Date registrationDate) {
          super(roleType, userName, password, registrationDate);
     }
}
