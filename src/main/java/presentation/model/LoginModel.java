package presentation.model;

import data.repository.AuthenticationRepository;

public class LoginModel {
    private AuthenticationRepository repository;

    public LoginModel(AuthenticationRepository repository) {
        this.repository = repository;
    }

    public boolean login(String username, String password){
        return repository.login(username,password);
    }

    public boolean checkForForceUpdate() {
        return repository.checkForForceUpdate();
    }
}
