package domain.entities;

public enum JobType {
    FULL_TIME, PART_TIME, INTERN, CONTRACT
}
