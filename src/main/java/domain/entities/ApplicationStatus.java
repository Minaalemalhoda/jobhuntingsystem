package domain.entities;

public enum ApplicationStatus {
    PENDING, MATCHED
}
