package domain.entities;

public enum RoleType {
    APPLICANT, COMPANY, ADMIN
}
