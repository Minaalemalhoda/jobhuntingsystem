package domain.entities;

import java.util.Date;

public class Company extends User {
    String name;
    String industry;
    String introduction;
    String size;

    public Company(RoleType roleType, String userName, String password, Date registrationDate) {
        super(roleType, userName, password, registrationDate);
    }
}
