package domain.entities;

import java.util.Date;
import java.util.List;

public class WorkExperience {
     String companyName = "";
     String jobTitle = "";
     Date startDate;
     Date endDate;
     List<String> responsibilities;
     boolean isCurrentJob;
}
