package presentation.controller;

import domain.entities.User;
import presentation.model.ProfileModel;
import presentation.view.ProfileView;

public class ProfileController {
    ProfileModel model;
    ProfileView view;

    public ProfileController(ProfileModel model, ProfileView view) {
        this.model = model;
        this.view = view;
        getProfile();
    }

    private void getProfile() {
        User user = model.getProfile();
        if (user == null) updateView(true);
        else updateView(false);
        view.showProfile(user);
    }

    public void submitProfile(User user) {
        model.updateProfile(user);
        updateView(false);
    }

    public void updateView(boolean isFailed) {
        if (isFailed)
            view.showErrorMessage();
        else
            view.showSuccessMessage();
    }
}
