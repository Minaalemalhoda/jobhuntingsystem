package domain.entities;

public class OpenPositionFilter {
    String location;
    JobType jobType;
    long salary;
    Seniority seniority;
    String title;
    boolean isRemote;
}
