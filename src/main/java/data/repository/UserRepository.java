package data.repository;

import domain.entities.User;

public interface UserRepository {

    User getProfile(String userToken);

    void submitProfile(String userToken, User user);
}
