package data.repository;

import data.datasource.UserRemoteDataSource;
import domain.entities.User;

public class UserRepositoryImp implements UserRepository{
    private UserRemoteDataSource dataSource;

    @Override
    public User getProfile(String userToken) {
        return dataSource.getProfile(userToken);
    }

    @Override
    public void submitProfile(String userToken,User user) {
        dataSource.submitProfile(userToken, user);
    }
}
