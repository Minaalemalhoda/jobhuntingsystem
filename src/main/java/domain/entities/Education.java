package domain.entities;

import java.util.Date;

public class Education {
     String universityName = "";
     String major = "";
     Date startDate;
     Date graduationDate;
     boolean isCurrentlySudying;
}
