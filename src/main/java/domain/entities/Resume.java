package domain.entities;

import java.util.List;

public class Resume {
     String name = "";
     String surname = "";
     Contact contact;
     List<WorkExperience> workExperiences;
     List<Education> education;
     List<Skill> skillSet;
     String summery = "";
     List<Certificate> certificates;

     public Resume(String name, String surname, Contact contact, List<WorkExperience> workExperiences, List<Education> education, List<Skill> skillSet, String summery, List<Certificate> certificates) {
          this.name = name;
          this.surname = surname;
          this.contact = contact;
          this.workExperiences = workExperiences;
          this.education = education;
          this.skillSet = skillSet;
          this.summery = summery;
          this.certificates = certificates;
     }
}