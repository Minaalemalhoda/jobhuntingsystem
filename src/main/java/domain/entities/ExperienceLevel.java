package domain.entities;

public enum ExperienceLevel {
    BEGINNER,
    INTERMEDIATE,
    ADVANCED;
}
