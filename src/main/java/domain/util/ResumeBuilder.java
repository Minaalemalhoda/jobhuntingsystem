package domain.util;

import domain.entities.*;

import java.util.List;

public class ResumeBuilder {
    String name = "";
    String surname = "";
    Contact contact;
    List<WorkExperience> workExperiences;
    List<Education> education;
    List<Skill> skillSet;
    String summery = "";
    List<Certificate> certificates;

    public ResumeBuilder(String name, String sureName) {
        this.name = name;
        this.surname = sureName;
    }

    public ResumeBuilder contact(Contact contact) {
        this.contact = contact;
        return this;
    }

    public ResumeBuilder workExperiences(List<WorkExperience> workExperiences) {
        this.workExperiences = workExperiences;
        return this;
    }

    public ResumeBuilder skillSet(List<Skill> skillSet) {
        this.skillSet = skillSet;
        return this;
    }

    public ResumeBuilder certificates(List<Certificate> certificates) {
        this.certificates = certificates;
        return this;
    }

    public ResumeBuilder summery(String summery) {
        this.summery = summery;
        return this;
    }

    public Resume build() {
        Resume resume = new Resume(name, surname, contact, workExperiences, education, skillSet, summery, certificates);
        return resume;
    }

}
