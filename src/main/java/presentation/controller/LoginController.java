package presentation.controller;

import presentation.model.LoginModel;
import presentation.view.LoginView;

public class LoginController {
    LoginModel model;
    LoginView view;

    public void login(String username, String password){
        boolean isSuccessful = model.login(username, password);
        updateView(!isSuccessful);
    }

    public void updateView(boolean isFailed){
        if(isFailed)
            view.showErrorMessage();
        else
            view.showSuccessMessage();
    }

}
