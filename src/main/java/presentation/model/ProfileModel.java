package presentation.model;

import data.repository.AuthenticationRepository;
import data.repository.UserRepository;
import domain.entities.User;

public class ProfileModel {
    private AuthenticationRepository authRepo;
    private UserRepository userRepo;

    public ProfileModel(AuthenticationRepository authRepo, UserRepository userRepo){
        this.authRepo = authRepo;
        this.userRepo = userRepo;
    }

    public void updateProfile(User user){
        userRepo.submitProfile(authRepo.getUserToken(), user);
    }

    public User getProfile(){
       return userRepo.getProfile(authRepo.getUserToken());
    }
}
