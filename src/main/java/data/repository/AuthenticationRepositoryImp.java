package data.repository;

import data.datasource.AuthenticationLocalDataSource;
import data.datasource.AuthenticationRemoteDataSource;
import domain.entities.User;

public class AuthenticationRepositoryImp implements AuthenticationRepository {
    private AuthenticationRemoteDataSource remoteDataSource;
    private AuthenticationLocalDataSource localDataSource;

    public AuthenticationRepositoryImp(AuthenticationRemoteDataSource remoteDataSource, AuthenticationLocalDataSource localDataSource) {
        this.remoteDataSource = remoteDataSource;
        this.localDataSource = localDataSource;
    }

    @Override
    public boolean login(String userName, String password) {
        return remoteDataSource.login(userName, password);
    }

    @Override
    public void register(User user) {
        remoteDataSource.register(user);
    }

    @Override
    public void deleteAccount(User user) {
        remoteDataSource.deleteAccount(user);
    }

    @Override
    public boolean checkForForceUpdate() {
        return localDataSource.checkForForceUpdate();
    }

    @Override
    public int getCachedLoginCounter() {
        return localDataSource.getCachedLoginCounter();
    }

    @Override
    public String getUserToken() {
        return "";
    }
}
