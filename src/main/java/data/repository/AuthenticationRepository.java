package data.repository;

import domain.entities.User;

public interface AuthenticationRepository {
    int LOGIN_COUNTER_FOR_FORCEUPDATE = 4;

    boolean login(String userName, String password);

    void register(User user);

    void deleteAccount(User user);

    boolean checkForForceUpdate();

    int getCachedLoginCounter();

    String getUserToken();
}
