package domain.entities;

import java.util.Map;

public class OpenPosition {
    Company company;
    String title;
    Seniority seniority;
    JobType jobType;
    boolean isRemote;
    Location location;
    long salary;
    Map<Applicant, ApplicationStatus> applicants;
}
