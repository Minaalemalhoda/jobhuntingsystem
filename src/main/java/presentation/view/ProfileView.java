package presentation.view;

import domain.entities.RoleType;
import domain.entities.User;
import presentation.controller.ProfileController;

public class ProfileView {
    private ProfileController controller;

    void onSubmitButtonClicked(RoleType roleType, String userName, String password) {
        controller.submitProfile(new User(roleType, userName, password, null));
    }

   public void showProfile(User user) {

    }

    public void showSuccessMessage() {
    }

    public void showErrorMessage() {
    }
}
