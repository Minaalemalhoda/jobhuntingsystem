package domain.entities;

public enum Seniority {
    JUNIOR, EXPERT, SENIOR, LEADER, DIRECTOR
}
